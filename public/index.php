<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();

    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages[] = 'Спасибо, результаты сохранены.';
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email']=!empty($_COOKIE['email_error']);
    $errors['sex']=!empty($_COOKIE['sex_error']);
    $errors['bio']=!empty($_COOKIE['bio_error']);
    $errors['check']=!empty($_COOKIE['check_error']);
    $errors['abil']=!empty($_COOKIE['abil_error']);
    $errors['year']=!empty($_COOKIE['year_error']);
    $errors['limb']=!empty($_COOKIE['limb_error']);
    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните имя корректно.</div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Заполните почту</div>';
    }
    if ($errors['sex']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sex_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите пол</div>';
    }
    if ($errors['bio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Введите биографию</div>';
    }
    if ($errors['check']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('check_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Подтвердите согласие</div>';
    }
    if ($errors['abil']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('abil_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите сверхспособность</div>';
    }
    if ($errors['year']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('year_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Корректно введите дату рождения</div>';
    }
    if ($errors['limb']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('limb_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div>Выберите количество конечностей</div>';
    }


    // TODO: тут выдать сообщения об ошибках в других полях.

    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['sex_value'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['bio_value'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];
    $values['check_value'] = empty($_COOKIE['check_value']) ? '' : $_COOKIE['check_value'];
    $values['abil_value'] = empty($_COOKIE['abil_value']) ? '' : $_COOKIE['abil_value'];
    $values['year_value'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['limb_value'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
    // TODO: аналогично все поля.

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])|| (preg_match("/^[a-z0-9_-]{2,20}$/i", $_POST['fio']))) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio2'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('sex_value', $_POST['radio2'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['textarea1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('bio_value', $_POST['textarea1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['checkbox'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('check_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('check_value', $_POST['checkbox'], time() + 365 * 24 * 60 * 60);
    }
    $kek=0;
    $myselect=$_POST['select1'];
    for($i=0;$i<5;$i++)
    {
        if($myselect[$i]!=1)
        {
            $myselect[$i]=0;
        }
        else
            $kek=1;
    }
    if (!$kek) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('abil_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('abil_value', $_POST['select1'], time() + 365 * 24 * 60 * 60);
    }
    if (!isset($_POST['radio1'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limb_value', $_POST['radio1'], time() + 365 * 24 * 60 * 60);
    }
    if (empty($_POST['birthyear'])||$_POST['birthyear']<1886 || $_POST['birthyear']>2021) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('year_value', $_POST['birthyear'], time() + 365 * 24 * 60 * 60);
    }


    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    } else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error','',100000);
        setcookie('sex_error','',100000);
        setcookie('bio_error','',100000);
        setcookie('check_error','',100000);
        setcookie('abil_error','',100000);
        setcookie('year_error','',100000);
        setcookie('limb_error','',100000);
        // TODO: тут необходимо удалить остальные Cookies.
    }


    /*$errors = FALSE;
    if (empty($_POST['fio'])) {
      print('Заполните имя.<br/>');
      $errors = TRUE;
    }
    if (empty($_POST['birthyear'])) {
      print('Заполните год рождения.<br/>');
      $errors = TRUE;
    }
    if (!isset($_POST['radio2'])) {
      print('Выберите пол.<br/>');
      $errors = TRUE;
    }
    if (!isset($_POST['radio1'])) {
      print('Выберите к-во конечностей.<br/>');
      $errors = TRUE;
    }
    if (!isset($_POST['checkbox'])) {
      print('Подтверите согласие.<br/>');
      $errors = TRUE;
    }


    if ($errors) {
      exit();
    }

    */
    $user = 'u20978';
    $pass = '6786355';
    $db = new PDO('mysql:host=localhost;dbname=u20978', $user, $pass, array(PDO::ATTR_PERSISTENT => true));



    $stmt = $db->prepare("INSERT INTO form (name, year,email,sex,limb,bio,checkbox) VALUES (:fio, :birthyear,:email,:sex,:limb,:bio,:checkbox)");
    $stmt -> execute(array('fio'=>$_POST['fio'], 'birthyear'=>$_POST['birthyear'],'email'=>$_POST['email'],'sex'=>$_POST['radio2'],'limb'=>$_POST['radio1'],'bio'=>$_POST['textarea1'],'checkbox'=>$_POST['checkbox']));


    $stmt=$db->prepare("INSERT INTO all_abilities(ability_god,ability_through_walls,ability_levity,ability_kostenko,ability_kolotiy) VALUES(:god,:wall,:levity,:kostenko,:kolotiy)");

    $stmt->execute(array('god'=>$myselect[0],'wall'=>$myselect[1],'levity'=>$myselect[2],'kostenko'=>$myselect[3],'kolotiy'=>$myselect[4]));


    setcookie('save', '1');
    header('Location: index.php');
}
